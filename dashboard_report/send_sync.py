import typing
import logging
import requests
import time
from json import dumps as to_json, loads as from_json
from os.path import exists as file_exists
from .const import DASHBOARD_URL
from .action import DashboardAction

if typing.TYPE_CHECKING:
    from .authsocket import Handshake

_LOGGER = logging.getLogger(__name__)


def _upload_post(url: str, action: DashboardAction, bearer_token: typing.Optional[str] = None) -> None:
    headers = {
        'Content-Type': 'application/json',
    }
    if bearer_token:
        headers['Authorization'] = 'Bearer ' + bearer_token

    r = requests.post(url, data=to_json(action.to_json()), headers=headers, timeout=30)
    if r.status_code != 200:
        raise Exception(f"Upload not accepted by the server: {r.status_code} - {r.text}")
    response = r.json()
    status = response['status']
    if status != 'ok':
        raise Exception(f"Upload not accepted by the server: {status}")


def _upload_websocket(url: str, action: DashboardAction, handshake: "Handshake") -> None:
    from contextlib import closing
    from websocket import create_connection

    with closing(create_connection(url, timeout=30)) as websocket:
        handshake.run_sync(websocket)
        websocket.send(to_json(action.to_json()))
        response = from_json(websocket.recv())
        status = response['status']
        if status != 'ok':
            raise Exception(f"Upload not accepted by the server: {status}")


def dashboard_action(action: DashboardAction,
                     unreported_exception: bool = False,
                     **kwargs) -> None:
    """Send an action to the dashboard

    This sends a constructed action to the dashboard.

    :param action: The dashboard action to apply
    :param unreported_exception: Raise an exception if unable to report
    :param kwargs: Arguments used for upload configuration
    """

    url = kwargs.get('url') or DASHBOARD_URL
    if not url:
        if unreported_exception:
            raise RuntimeError("No dashboard reporting URL configured")
        if action.failed:
            _LOGGER.error(f"Unable to report dashboard failure for {action.code}")
        else:
            _LOGGER.info(f"Unable to report dashboard success for {action.code}")
        return

    retries = kwargs.get('upload_retries', 4)

    _LOGGER.debug(f"Sending dashboard report for {action.code} to {url}")

    if url.startswith('ws'):
        key = kwargs.get('key')
        if key is None:
            if unreported_exception:
                raise RuntimeError("No dashboard reporting private key configured")
            if action.failed:
                _LOGGER.error(f"No key to report dashboard failure for {action.code}")
            else:
                _LOGGER.info(f"No key to report dashboard success for {action.code}")
            return

        try:
            from .authsocket import Handshake
            handshake = Handshake(key)
        except ImportError as e:
            if unreported_exception:
                raise RuntimeError("Unable to load cryptography, make try installing with [auth]") from e
            if action.failed:
                _LOGGER.error(f"Failed to load cryptography for failure of {action.code}", exc_info=True)
            else:
                _LOGGER.info(f"Failed to load cryptography for success of {action.code}", exc_info=True)
            return

        for t in range(retries):
            try:
                _upload_websocket(url, action, handshake)
                _LOGGER.debug(f"Dashboard report complete for {action.code}")
                return
            except:
                _LOGGER.debug(f"Dashboard report initial try failed for {action.code}", exc_info=True)
            time.sleep(10)

        try:
            _upload_websocket(url, action, handshake)
            _LOGGER.debug(f"Dashboard report complete for {action.code}")
        except:
            if unreported_exception:
                raise
            if action.failed:
                _LOGGER.error(f"Error during dashboard failure report for {action.code}", exc_info=True)
            else:
                _LOGGER.warning(f"Error during dashboard success report for {action.code}", exc_info=True)
        return

    bearer_token = kwargs.get('bearer_token')
    if bearer_token and file_exists(bearer_token):
        with open(bearer_token, 'r') as f:
            bearer_token = f.read().strip()

    for t in range(retries):
        try:
            _upload_post(url, action, bearer_token)
            _LOGGER.debug(f"Dashboard report complete for {action.code}")
            return
        except:
            _LOGGER.debug(f"Dashboard report initial try failed for {action.code}", exc_info=True)
        time.sleep(10)

    try:
        _upload_post(url, action, bearer_token)
        _LOGGER.debug(f"Dashboard report complete for {action.code}")
    except:
        if unreported_exception:
            raise
        if action.failed:
            _LOGGER.error(f"Error during dashboard failure report for {action.code}", exc_info=True)
        else:
            _LOGGER.warning(f"Error during dashboard success report for {action.code}", exc_info=True)


def dashboard_report(code: str,
                     station: typing.Optional[str] = None,
                     **kwargs) -> None:
    """Send a report to the dashboard

    This constructs an action from the arguments, then sends
    it to the dashboard.

    :param code: The dashboard reporting code
    :param station: The station code, if applicable
    :param failed: If set then update the failure status
    :param kwargs: Arguments used in action construction
    """
    action = DashboardAction.from_args(station, code, **kwargs)
    return dashboard_action(action, **kwargs)


def report_ok(code: str, station: typing.Optional[str] = None, **kwargs) -> None:
    """Report nominal operation to the dashboard

    Use this function to report normal operation of the dashboard entry.
    This is normally used on completion of an automatic process if
    no problems where encountered.

    :param code: The dashboard reporting code
    :param station: The station code, if applicable
    :param unreported_exception: Raise an exception if unable to report
    :param kwargs: Arguments used in action construction
    """
    return dashboard_report(code, station, failed=False, **kwargs)


def report_failed(code: str, station: typing.Optional[str] = None, **kwargs) -> None:
    """Report failure to the dashboard

    Use this function to report failure of the dashboard entry.
    This is normally used when a problem is encountered during an
    automatic process.  If the problem arises from an exception,
    set exc_info=True argument to capture the Python stack trace.

    :param code: The dashboard reporting code
    :param station: The station code, if applicable
    :param unreported_exception: Raise an exception if unable to report
    :param kwargs: Arguments used in action construction
    """
    return dashboard_report(code, station, failed=True, **kwargs)