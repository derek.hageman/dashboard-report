MAJOR_VERSION = 1
MINOR_VERSION = 1
PATCH_VERSION = 0
__short_version__ = f'{MAJOR_VERSION}.{MINOR_VERSION}'
__version__ = f'{__short_version__}.{PATCH_VERSION}'

DASHBOARD_URL = 'https://gml.noaa.gov/aero/dataview/dashboard/control/update'
