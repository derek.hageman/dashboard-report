import typing
from base64 import b64decode, b64encode
from os.path import exists as file_exists
from json import dumps as to_json, loads as from_json
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey as PrivateKey, Ed25519PublicKey as PublicKey
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat

if typing.TYPE_CHECKING:
    import aiohttp
    import websocket


def key_to_bytes(key: PublicKey) -> bytes:
    return key.public_bytes(Encoding.Raw, PublicFormat.Raw)


class Handshake:
    def __init__(self, private_key):
        if file_exists(private_key):
            with open(private_key, 'rb') as f:
                key = f.read()
            if len(key) == 32:
                private_key = PrivateKey.from_private_bytes(key)
            else:
                private_key = PrivateKey.from_private_bytes(b64decode(key.decode('ascii').strip()))
        else:
            private_key = PrivateKey.from_private_bytes(b64decode(private_key))
        self.private_key = private_key

    def _handshake(self) -> typing.Dict[str, typing.Any]:
        return {
            'public_key': b64encode(key_to_bytes(self.private_key.public_key())).decode('ascii'),
        }

    def _response(self, challenge: typing.Dict[str, typing.Any]) -> typing.Dict[str, typing.Any]:
        token = b64decode(challenge['token'])
        signature = self.private_key.sign(token)
        return {
            'signature': b64encode(signature).decode('ascii'),
        }

    async def run_async(self, websocket: "aiohttp.client.ClientWebSocketResponse") -> None:
        await websocket.send_json(self._handshake())
        challenge = await websocket.receive_json()
        await websocket.send_json(self._response(challenge))

    def run_sync(self, websocket: "websocket.WebSocket") -> None:
        websocket.send(to_json(self._handshake()))
        challenge = from_json(websocket.recv())
        websocket.send(to_json(self._response(challenge)))
