"""Forge dashboard reporting tools.

This module contains tools for reporting to the Forge status
dashboard. The dashboard itself is a high level monitoring interface
intended for quick viewing about the overall status of various
subsystems or processes that run automatically.


Functions
---------
report_ok(code, station=None, **kwargs)
    Report nominal operation for code on an optional station.
report_failed(code, station=None, **kwargs)
    Report failure for code on an optional station.
report_ok_async(code, station=None, **kwargs)
    Report nominal operation for code on an optional station
    using asyncio.
report_failed_async(code, station=None, **kwargs)
    Report failure for code on an optional station using
    asyncio.


Station Requirements
--------------------
When not present, the station refers to a common shared station.
This mode of operation should be used when a process is not
specific to a station.

When provided, the station is ASCII case-insensitive, but must start
with a letter.  If it consists of exactly three letters, then it
refers to a GAW station so care must be taken to ensure that
any internal codes are mapped to correct GAW stations.  Any other
station up to 32 characters in length is allowed, the only
character outside a letter or number permitted is an underscore.


Code Requirements
--------------------
The reporting code must be provided for all updates.  This is an
internal identifier used to associate the state of the report
in the main dashboard.  It is ASCII case-insensitive and must
start with a letter.  It may contain letters, dashes, and
underscores.

In general, the code should follow the same dash-namespaced
format as Forge visualization modes, since the access control
is shared between the two.  This means it should usually
be of the form <MODE>-<LEVEL>-<IDENTIFIER>.  For example:
aerosol-raw-ingest-auxiliary.  In general, a user would have
access to "aerosol-*" so they would also be able to see
this dashboard entry.


Common Information Contents
---------------------------
Additional report information (notifications, watchdogs,
events, and conditions) share several components.  The
code is a code with the same requirements as the common
dashboard codes, but is not subject to the namespacing
(only within the specific type of dashboard entry).  The
severity is either info, warning, or error and designates
how the information is presented and what level it triggers
emails at.  The data is an optional string of additional
data attached to the information.  For simple formatting
this is just text displayed with the information when it is
active.  If multiple lines are present, the default HTML
formatting will use a <pre> block which will preserve line
spacing as fixed width.

As an exception to the code requirements, notifications
accept an empty code.  The empty code notification, if
present, displays as global status at the top of the
dashboard entry information.

When information is parsed directly from a string instead
of a dict, the components are separated by ":", with
missing ones using defaults or empty.


Common Reporting Arguments
-------------------------
The reporting function accept a generic **kwargs allowing
for arbitrary named arguments to be set.  These are used
to construct the report in detail or to configure the
specifics of how the reporting is done.

notifications (iterable[str,dict]): The notifications to include with the update
watchdogs (iterable[str,dict]): The watchdogs to start or restart, may also include a last_seen (float) as the epoch time the watchdog was updated
events (iterable[str,dict]): Events (occurred_at as the second component) added as part of the update
conditions (iterable[str,dict]): Conditions (start_time, end_time as the second and third components) set as part of the update
exc_info (bool): Place the Python exception stack trace into the status notification
update_time (str ISO8601, datetime.datetime, float seconds): Override the time of update, when omitted the current time is used
preserve_existing_notifications (bool): When set, any existing notifications are NOT cleared as part of the update
notifications_to_clear (iterable[str]): Specific notifications to clear, implies preserve_existing_notifications
watchdogs_to_clear (iterable[str]): The watchdogs to stop
unbounded_time (bool): Disable sanity limits on times, allowing for updates that move backwards or report in the future
url (str): Override the URL to report to
key (str): The private key or file name used for websocket authentication
bearer_token (str): A bearer token used for POST authentication if required

"""


from .send_sync import report_ok, report_failed
from .send_async import report_ok as report_ok_async, report_failed as report_failed_async
