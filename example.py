from dashboard_report import report_ok, report_failed


def do_things(success):
    if not success:
        raise RuntimeError("Failed operation")
    print("Successful operation")


try:
    do_things(True)
    report_ok("example-ok", station="bld")
except:
    report_failed("example-ok", station="bld", exc_info=True)


try:
    do_things(False)
    report_ok("example-failed", station="mlo")
except:
    report_failed("example-failed", station="mlo", exc_info=True)
