# Introduction

This package provides tools to report status to the [Forge dashboard](https://gml.noaa.gov/aero/dataview/dashboard/) (requires being on the GML internal network or being logged in as a user with access).  For an example of what a populated dashboard looks like, see [here](https://gml.noaa.gov/aero/dataview/dashboard/example).

The dashboard itself provides a high level overview of the status of systems.  Each entry is either nominal if it last reported ok status and has reported recently (generally within the last day), if it has not reported recently it is considered offline, and if it has reported an explicit failure then that status is available.  It also allows the user to set up emails to be sent when conditions of a given severity or worse are detected.


# Quick Start

This package can be installed directly into a Python virtual environment with pip:

```shell
pip install git+https://gitlab.com/derek.hageman/dashboard-report.git
```

Once installed, the simplest form of reporting is to run the process within a blanket try-catch and report failure if any exception is raised.

```python
from dashboard_report import report_ok, report_failed

# ...

try:
    do_the_processing()
    report_ok("example-report", station="brw")
except:
    report_failed("example-report", station="brw", exc_info=True)
```

## Alternative Installation Methods

If not using a virtual environment the package can also simply be placed into a project directory.  For example, using a git submodule within a version controlled directory:

```shell
git submodule add https://gitlab.com/derek.hageman/dashboard-report.git dashboard_report
```

Cloned directly:

```shell
git clone https://gitlab.com/derek.hageman/dashboard-report.git dashboard_report
```

## Command Line

If installed with pip in a virtual environment the command `dashboard-report` is also added.  This command allows for simple reporting to the dashboard through non-python scripts (e.x. at the end of a shell script).  To use this without activating the virtual environment, you can also specify the full path to it in the reporting script:

```shell
# ...
/path/to/root/venv/bin/dashboard-report ok example-shell-report
```


# Status Display

In addition to the full status display and configurable email reporting, there is a mechanism for showing a quick status of a specific dashboard entry on an external website.  This takes the form of either a JSON response suitable for dynamic status display queried from some Javascript or as a image badge that can be used directly in an overview or readme.

## Badge

To show a status badge, use a URL like `https://gml.noaa.gov/aero/dataview/dashboard/badge/example-ok/brw.svg` (for the code `example-ok` and station `brw`) or for a code not associated with a station `https://gml.noaa.gov/aero/dataview/dashboard/badge/example-ok.svg`.  This will generate the image ![Ok badge](https://gml.noaa.gov/aero/dataview/dashboard/badge/example-ok/brw.svg).  This can be further customized with the query parameters `label`, `ok`, `failed`, and `offline` to set the text for various states.  So `https://gml.noaa.gov/aero/dataview/dashboard/badge/example-failed.svg?label=Example%20Status&failed=ERROR` produces ![Failed badge](https://gml.noaa.gov/aero/dataview/dashboard/badge/example-failed.svg?label=Example%20Status&failed=ERROR).

## JSON

A simple JSON response for the status of an entry if reported with a GET request to `https://gml.noaa.gov/aero/dataview/dashboard/badge/example-ok/brw.json` (for the code `example-ok` and station `brw`) or for a code not associated with a station `https://gml.noaa.gov/aero/dataview/dashboard/badge/example-ok.json`.  The generates JSON like:

```json
{
  "status": "ok"
}
```

The status field can be one of `ok`, `failed`, or `offline`.


# High Level Reporting

At the most basic level, reporting consists of reporting a status (either ok or failed) at a specific time.  By default, if no report at all is seen for a day, the reporting entry is then considered offline, regardless of the previously reported status.  A reporting entry is uniquely identified by a station and entry code.  The station may be empty to designate a report not associated with a specific station (e.x. the website status).  

The station code consists of one to 32 ASCII letters, numbers, or underscore but must begin with a letter and is case-insensitive.  Any station code of exactly three letters must be a valid GAW station, which may be different from the internal coding used for the same station code (e.x. "BAO" as a surface radiation site is "BOU" in the [GAW database](https://gawsis.meteoswiss.ch/GAWSIS/#/search/station/stationReportDetails/0-20008-0-BOU)).  Other codes, or codes with numbers do not need to follow this standard (e.x. the mobile radiation platform uses MRAD as a station)

The entry code consists of one to 64 ASCII letters, numbers, underscore, or dash but must begin with a letter and is case-insensitive.  The entry code is used with the general Forge access control and dash-hierarchical namespacing.  So in general, it should consist of high level access type separated by dashes for more specific levels of data or subsystems.  For example, ingesting the scaled radiation data might report as `radiation-raw-ingest-scaled`, while the aerosol acquisition system processing might be `acquisition-ingest`.  This is so that a user granted access to `radiation-*` would see the radiation ingest that they might be interested in without seeing the various aerosol processing that they do not care about.


# Detailed Reporting

In addition to reporting high level status (ok or failed) the dashboard allows reporting details about operational status or other detected conditions.  These may be reported through additional parameters to the reporting Python functions or with arguments to the command line interface.  See the example at the very top of this document for how these are displayed by default.

Each component of detailed information has a code identifying that consists one to 64 ASCII letters, numbers, underscore, or dash but must begin with a letter and is case-insensitive.  Unlike the general entry code, these do not share any sort of namespacing, so they only need to identify parts uniquely within the entry.  For example, incoming file processing might generate events with the code `file-processed` for each file ingested.

Additionally, each informational detail has a severity associated with it.  This severity is one of `INFO`, `WARNING`, or `ERROR`.  The severity determines how the detail is displayed (e.x. the color of the background) and how it affects the total status of the dashboard entry.  So, an entry with a `WARNING` level notification will send emails to any users subscribed to warning or higher, even if the total dashboard entry is reporting ok.

Finally, there is an optional data field with each detail.  By default, if this is provided, it is shown as text along with the detail code.  If it contains multiple lines, then it is formatted with `<pre>` block, so it is monospace and preserves white space; this is useful to encode an exception stack trace.

## Notifications

The simplest form of detailed reporting is notifications.  These are simply messages that indicate the status of the dashboard entry at the last time it was run.  There is also a special notification with an empty code that is used to display the "global status" of the entry.  This is commonly used to display a Python exception stack trace on failure.

To report notifications, use the `notifications` keyword argument:

```python
report_ok("example-report", notifications=[
    {"code": "first-notification", "severity": "info", "data": "Notification message"},
])
```

## Watchdogs

A watchdog is a form of status that reports only if it is not refreshed within a time limit.  This is a kind of inverse of the notification in that it only shows
when not set on time.  For example, when a specific aerosol instrument is processed a watchdog will be started specific to that instrument, allowing a report if that instrument goes offline, even if the rest of the data continues to come in.

To start or refresh a watchdog, use the `watchdogs` keyword argument:

```python
report_ok("example-report", watchdogs=[
    {"code": "instrument-online-A11", "severity": "error"},
])
```

## Conditions

A condition is a state spanning a start and end time.  Conditions are reported when they are present, with information about how long the condition was present for.  For example, an instrument reporting abnormal status flags might set a condition while the flags are abnormal.

To set condition for a period of time, use the `conditions` keyword argument:

```python
event_start = EPOCH_START
event_end = EPOCH_END
# ...
report_ok("example-report", conditions=[
    {"code": "abnormal-flag", "severity": "warning", "start_time": event_start, "end_time": event_end},
])
```

## Events

An event is a piece of status indicating that something happened at a specific point in time.  Unlike other forms of status, they are not combined or replaced by subsequent updates.  Instead, the default display shows a time ascending list of events that occurred and the time the event happened.  For example, this may be used by aerosol processing to display user entered log messages.  If no time is provided, it is set to the time of the report.

To add events, use the `events` keyword argument:

```python
report_ok("example-report", events=[
    {"code": "message-log", "severity": "info", "data": "User message log", "occurred_at": time.time()},
])
```