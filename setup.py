#!/usr/bin/python3

import os
from setuptools import setup, find_packages
from dashboard_report import const

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md')) as readme:
    LONG_DESCRIPTION = readme.read()

with open(os.path.join(here, 'requirements.txt')) as requirements_txt:
    REQUIRES = requirements_txt.read().splitlines()

setup(
    name='dashboard_report',
    version=const.__version__,
    license='MIT',
    author='Derek Hageman',
    author_email='derek.hageman@noaa.gov',
    description="Tools to inform the Forge dashboard about status",
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    install_requires=REQUIRES,
    extras_require={
        'auth': [
            "cryptography>=36.0.0",
            "websocket-client>=1.5.0",
        ],
    },
    python_requires='>=3.6,<4.0',
    test_suite="tests",
    entry_points={"console_scripts": [
        "dashboard-report = dashboard_report.cli:main",
    ]},
    packages=find_packages(),
)
